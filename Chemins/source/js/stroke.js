window.onload = function() {
  var stroke_color = "black";
  var baseline = 0;
  var x_height = 500;
  //33 : point d'exclamation
  var c33= document.getElementById('c33');
  paper.setup(c33);
  const path33 = new paper.CompoundPath();
  var path33_1 = new paper.Path();
  path33_1.fillColor = stroke_color;
  var path33_2 = new paper.Path();
  path33_2.fillColor = stroke_color;
  path33.data.unicode = 33;
  path33.data.name = "exclam";
  path33.addChild(path33_1);
  path33.addChild(path33_2);
  var p33_1 = new paper.Point(100, 0);
  var p33_2 = new paper.Point(130,0);
  var p33_3 = new paper.Point(130,600);
  var p33_4 = new paper.Point(100,600);
  var p33_5 = new paper.Point(100, 700);
  var p33_6 = new paper.Point(130,700);
  var p33_7 = new paper.Point(130, 670);
  var p33_8 = new paper.Point(100, 670);
  path33_1.moveTo(p33_3);
  path33_1.lineTo(p33_2);
  path33_1.lineTo(p33_1);
  path33_1.lineTo(p33_4);
  path33_1.lineTo(p33_3);
  path33_2.moveTo(p33_5);
  path33_2.lineTo(p33_6);
  path33_2.lineTo(p33_7);
  path33_2.lineTo(p33_8);
  path33_2.lineTo(p33_5);
  //path33.closePath();
  //path33.flatten(3);
  //console.log(path33.children[1].segments);

  // 40
  var c40= document.getElementById('c40');
  paper.setup(c40);
  const path = new paper.CompoundPath();
  path.strokeColor = 'black';
  path.data.unicode = 40;
  path.data.name = "parenleft";
  var haut = new paper.Point(200, 700);
  var milieu = new paper.Point(0,350);
  var bas = new paper.Point(200,0);
  var bas2 = new paper.Point(220,0);
  var haut2 = new paper.Point(220, 700);
  var milieu2 = new paper.Point(20,350);
  path.moveTo(haut);
  path.arcTo(milieu,bas);
  path.lineTo(bas2);
  path.arcTo(milieu2, haut2);
  path.closePath();
  path.flatten(3);
  // var pData = path.pathData;
  // //console.log(path.segments);
  // var pData_cut = pData.split(/(?=[A-z])/);
  //
  // var path_a = [];
  // for(let i=0; i<pData_cut.length; i++) {
  //   for(let j=0; j<path.segments.length; j++) {
  //     let type = pData_cut[i].charAt(0);
  //     if(type == "l") {
  //       type = "L";
  //     }
  //     path_a.push({type:type, x:path.segments[j].point.x, y:(-(path.segments[j].point.y))+700});
  //   }
  //
  //
  //
  // }
//console.log(path_a);




// 40
var c41= document.getElementById('c41');
paper.setup(c41);
const path41 = new paper.CompoundPath();
path41.strokeColor = 'black';
path41.data.unicode = 41;
path41.data.name = "parenright";
var haut = new paper.Point(0, 700);
var milieu = new paper.Point(200,350);
var bas = new paper.Point(0,0);
var bas2 = new paper.Point(20,0);
var haut2 = new paper.Point(20, 700);
var milieu2 = new paper.Point(220,350);
path41.moveTo(haut);
path41.arcTo(milieu,bas);
path41.lineTo(bas2);
path41.arcTo(milieu2, haut2);
path41.closePath();
path41.flatten(3);

paper.view.draw();

var notdefPath = new opentype.Path();
notdefPath.moveTo(100, 0);
notdefPath.lineTo(100, 700);
notdefPath.lineTo(600, 700);
notdefPath.lineTo(600, 0);
notdefPath.moveTo(200, 100);
notdefPath.lineTo(500, 100);
notdefPath.lineTo(500, 600);
notdefPath.lineTo(200, 600);
var notdefGlyph = new opentype.Glyph({
    name: '.notdef',
    unicode: 0,
    advanceWidth: 650,
    path: notdefPath
});

const glyphs = [notdefGlyph];

var glyphs_paths = [path33, path, path41];

for (let p of glyphs_paths) {
  var o_path = new opentype.Path();
  o_path.stroke = "black";
  var pData = p.pathData;
  var pData_cut = pData.split(/(?=[A-z])/);
  var path_new = [];
  for (let c of p.children) {
      for(let j=0; j<c.segments.length; j++) {
        console.log("p" + c.segments.length);
        if(j<1) {
          path_new.push({type:"M", x:c.segments[j].point.x, y:(-(c.segments[j].point.y))+700});
        } else {
          path_new.push({type:"L", x:c.segments[j].point.x, y:(-(c.segments[j].point.y))+700});
        }

      }

    }
  o_path.commands = path_new;

  var o_glyph = new opentype.Glyph({
    name: p.data.name,
    unicode: p.data.unicode,
    advanceWidth: 650,
    path: o_path
  });


  glyphs.push(o_glyph);
  console.log(glyphs);
}



//passer le path à opentype.js
// const p33 = new opentype.Path();
// const p40 = new opentype.Path();
// aPath.stroke = "black";
// aPath.fill = null;
// aPath.commands = path_a;
//console.log(aPath);

// var canvas = document.getElementById('canvas2');
// var ctx = canvas.getContext('2d');



  // const glyph33 = new opentype.Glyph({
  //     name: 'exclam',
  //     unicode: 33,
  //     advanceWidth: 650,
  //     path: aPath
  // });
  //
  // const glyph40 = new opentype.Glyph({
  //     name: 'parenleft',
  //     unicode: 40,
  //     advanceWidth: 650,
  //     path: aPath
  // });
  //
  // const glyph41 = new opentype.Glyph({
  //     name: 'parenright',
  //     unicode: 40,
  //     advanceWidth: 650,
  //     path: aPath
  // });


const font = new opentype.Font({
    familyName: 'OpenTypeSans',
    styleName: 'Medium',
    unitsPerEm: 1000,
    ascender: 800,
    descender: -200,
    glyphs: glyphs});




        // var x = 50;
        // var y = 120;
        // var fontSize = 72;
        // glyphs[1].draw(ctx, x, y, fontSize);
        // glyphs[1].drawPoints(ctx, x, y, fontSize);
        // glyphs[1].drawMetrics(ctx, x, y, fontSize);

font.download();


}

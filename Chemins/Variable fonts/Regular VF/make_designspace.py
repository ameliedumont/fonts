import os

from fontTools.designspaceLib import DesignSpaceDocument, AxisDescriptor, SourceDescriptor, InstanceDescriptor

root = os.getcwd()
doc = DesignSpaceDocument()

familyName = "ReHershey"

#------
# axes
#------

a1 = AxisDescriptor()
a1.maximum = 1000
a1.minimum = 0
a1.default = 0
a1.name = "width"
a1.tag = "wdth"
doc.addAxis(a1)

a2 = AxisDescriptor()
a2.maximum = 1000
a2.minimum = 0
a2.default = 0
a2.name = "difference"
a2.tag = "dfrc"
doc.addAxis(a2)

#---------
# masters
#---------

s0 = SourceDescriptor()
s0.path = "ReHersheyNarrowerOK.ufo"
s0.name = "master.ReHersheyNarrowOK.0"
s0.familyName = familyName
s0.styleName = "Narrow"
s0.location = dict(difference=0, width=0)
s0.copyLib = True
s0.copyInfo = True
s0.copyGroups = True
s0.copyFeatures = True
doc.addSource(s0)

s1 = SourceDescriptor()
s1.path = "ReHersheyWiderOK.ufo"
s1.name = "master.ReHersheyWiderOK.1"
s1.familyName = familyName
s1.styleName = "Wider"
s1.location = dict(difference=0, width=1000)
doc.addSource(s1)

s2 = SourceDescriptor()
s2.path = "ReHersheyWiderOK_diff.ufo"
s2.name = "master.ReHersheyWiderOK_diff.2"
s2.familyName = familyName
s2.styleName = "Widerdiff"
s2.location = dict(difference=1000, width=1000)
doc.addSource(s2)

s3 = SourceDescriptor()
s3.path = "ReHersheyNarrowerOK_diff.ufo"
s3.name = "master.ReHersheyNarrowerOK_diff.3"
s3.familyName = familyName
s3.styleName = "Narrowerdiff"
s3.location = dict(difference=1000, width=0)
doc.addSource(s3)

#-------
# rules
#-------

# TODO: add rules to match the contents of `MutatorSans.designspace`
# http://github.com/LettError/mutatorSans/blob/master/MutatorSans.designspace

#--------
# saving
#--------

path = os.path.join(root, "ReHershey__test123__.designspace")
doc.write(path)



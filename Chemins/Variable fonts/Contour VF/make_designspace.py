import os

from fontTools.designspaceLib import DesignSpaceDocument, AxisDescriptor, SourceDescriptor, InstanceDescriptor

root = os.getcwd()
doc = DesignSpaceDocument()

familyName = "ReHershey"

#------
# axes
#------

a1 = AxisDescriptor()
a1.maximum = 1000
a1.minimum = 0
a1.default = 0
a1.name = "contrast"
a1.tag = "cntr"
doc.addAxis(a1)

a2 = AxisDescriptor()
a2.maximum = 1000
a2.minimum = 0
a2.default = 0
a2.name = "italic"
a2.tag = "ital"
doc.addAxis(a2)

#---------
# masters
#---------

s0 = SourceDescriptor()
s0.path = "ReHersheyContourOK.ufo"
s0.name = "master.ReHersheyContourOK.0"
s0.familyName = familyName
s0.styleName = "Contour"
s0.location = dict(italic=0, contrast=0)
s0.copyLib = True
s0.copyInfo = True
s0.copyGroups = True
s0.copyFeatures = True
doc.addSource(s0)

s1 = SourceDescriptor()
s1.path = "ReHersheyContrasteOK.ufo"
s1.name = "master.ReHersheyContrasteOK.1"
s1.familyName = familyName
s1.styleName = "Contraste"
s1.location = dict(italic=0, contrast=1000)
doc.addSource(s1)

s2 = SourceDescriptor()
s2.path = "ReHersheyContrasteOK_ital.ufo"
s2.name = "master.ReHersheyContrasteOK_ital.2"
s2.familyName = familyName
s2.styleName = "ContrasteItal"
s2.location = dict(italic=1000, contrast=1000)
doc.addSource(s2)

s3 = SourceDescriptor()
s3.path = "ReHersheyContourOK_ital.ufo"
s3.name = "master.ReHersheyContourOK_ital.3"
s3.familyName = familyName
s3.styleName = "ContourItal"
s3.location = dict(italic=1000, contrast=0)
doc.addSource(s3)

#-------
# rules
#-------

# TODO: add rules to match the contents of `MutatorSans.designspace`
# http://github.com/LettError/mutatorSans/blob/master/MutatorSans.designspace

#--------
# saving
#--------

path = os.path.join(root, "ReHershey__test123__.designspace")
doc.write(path)



# Chemins

This font family is made of several single paths and basic geometric shapes. The fonts were designed in Javascript with Paper.js.

There is a varaiable font version for the regular font.

![Chemins in use 01](chemins_in_use01.png "Chemins in use 01")
![Chemins in use 02](chemins_in_use02.jpeg "Chemins in use 02")
![Chemins in use 03](chemins_in_use03.JPG "Chemins in use 03")

## Make a variable font with free softwares only

You need Fontforge and two Python modules to install: AFDKO and fontmake. The easiest way is to create a virtual environment where you'll make your install.
Test that your fonts are compatible by using the interpolation tool in Fontforge. It's not ok as long as you have errors in the console.
For each font you must set in Element > Font Info > OS/2 > Metrics the xHeight and capHeight. If you do not it won't work.

Then adapt the make_designspace.py file with your fonts and axis you want to set. Also adapt the command.sh file and launch it. If everything is ok your variable font is generated.

Original explanations in [this video](https://www.youtube.com/watch?v=xoQuWARCUWI)

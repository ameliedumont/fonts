# Ductus

Ductus is a font based on traditional roman calligraphic paths. I transcripted the original paths of the calligraphic tool with Metapost and applied several pen shapes on it.
Then there is a monospace version in which the interpretation of the ductus is more free.

![Ductus in use 01](ductus_in_use01.png "Ductus in use 01")
![Ductus in use 02](ductus_in_use02.png "Ductus in use 02")
![Ductus in use 03](ductus_in_use03.png "Ductus in use 03")

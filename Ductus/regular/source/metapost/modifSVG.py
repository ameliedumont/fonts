import fontforge
import glob
import lxml.etree as ET

svg_final = glob.glob('svg' + '/*svg')
print(svg_final)

for files in svg_final:
    with open(files, 'rt') as f:
        tree = ET.parse(f)
    root = tree.getroot()
    new_style = "stroke:rgb(0.000000%,0.000000%,0.000000%); stroke-width: 0.000000;stroke-linecap: round;stroke-linejoin: round;stroke-miterlimit: 10.000000;fill: none;"
    root.set('width', '350')
    root.set('height', '550')
    root.set('viewBox', '0 0 350 550')
    for path in root.iter():
        style = path.attrib.get('style')
        if style:
            path.set('style', new_style)

    letter_svg = files.split("/")[-1].replace(".svg", "")
    tree.write(letter_svg + '.svg')

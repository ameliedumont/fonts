import fontforge
import glob

font = fontforge.font()
svg_final = glob.glob('new_svg' + '/*svg')


for letter_svg in svg_final:
    letter = letter_svg.split("/")[-1].replace(".svg", "")
    print(letter)

    glyph = font.createChar(int(letter))
    glyph.importOutlines(letter_svg)
    glyph.importOutlines(letter_svg)


font.fontname="noStroke"
font.save("noStroke.sfd")

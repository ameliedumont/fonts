# MetaAccanthis

This font is a revival of Accanthis font, present in the LaTeX font catalog. It was re-designed with Metapost.

![MetaAccanthis in use 01](font_in_use.png "MetaAccanthis")


# Fablab

Fablab is a typeface designed in FreeCad (free software for 3D modelling). The shapes of the letters were thought to be fine for CNC milling, 3D printing and lasercut.

![Fablab in use 01](fablab_in_use01.svg "Fablab in use 01")
![Fablab in use 02](fablab_in_use_02.pdf "Fablab in use 02")

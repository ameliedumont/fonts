# MetaFraktur

This font family was a second test about calligraphic ductus translated with Metapost, this time based on gothic letters. It is inspired by Fraktur and Textura gothic calligraphy.

![MetaFraktur in use 01](metafraktur01.png "MetaFraktur01")
![MetaFraktur in use 02](metafraktur02.png "MetaFraktur02")
![MetaFraktur in use 03](metafraktur03.png "MetaFraktur03")
![MetaFraktur in use 04](metafraktur04.png "MetaFraktur04")

